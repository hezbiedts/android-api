package com.edts.quotelist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.edts.quotelist.network.ApiConfig
import com.edts.quotelist.network.QuoteActivity
import com.edts.quotelist.network.response.Quote
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var rvQuote: RecyclerView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvQuote = findViewById(R.id.rvQuotes)
        progressBar = findViewById(R.id.progressBar)

        getList()

    }

    private fun showList(list: List<Quote>) {
        if (!list.isNullOrEmpty()) {
            val quoteAdapter = QuoteAdapter(list)
            val divider = DividerItemDecoration(this@MainActivity, RecyclerView.VERTICAL)

            with(rvQuote) {
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@MainActivity)
                adapter = quoteAdapter
                addItemDecoration(divider)
            }

            quoteAdapter.setOnItemClickCallback(object : QuoteAdapter.OnItemClickCallback {
                override fun onItemClicked(data: Quote) {
                    val intent = Intent(this@MainActivity, QuoteActivity::class.java)
                    intent.putExtra(QuoteActivity.EXTRA_DATA, data)
                    startActivity(intent)
                    Log.d(MainActivity::class.simpleName, "select quote of ${data.author}")
                }
            })
        }
    }

    private fun getList() {
        val apiService = ApiConfig.provideApiService()
        val call = apiService.getList()
        call.enqueue(object : Callback<List<Quote>> {
            override fun onResponse(call: Call<List<Quote>>, response: Response<List<Quote>>) {
                progressBar.visibility = View.INVISIBLE
                val dataArray = response.body()?.toMutableList()
                showList(dataArray!!)
            }

            override fun onFailure(call: Call<List<Quote>>, t: Throwable) {
                progressBar.visibility = View.INVISIBLE
                Toast.makeText(this@MainActivity, t.message, Toast.LENGTH_SHORT).show()
            }

        })
    }
}