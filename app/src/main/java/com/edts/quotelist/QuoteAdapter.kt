package com.edts.quotelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.edts.quotelist.network.response.Quote

class QuoteAdapter internal constructor(private val list: List<Quote>): RecyclerView.Adapter<QuoteAdapter.QuoteViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.adapter_list_item, parent, false)
        return QuoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuoteViewHolder, position: Int) {
        val quote = list[position]
        with(holder) {
            tvTitle.text = quote.en
            tvAuthor.text = quote.author
            itemView.setOnClickListener {
                onItemClickCallback.onItemClicked(quote)
            }
        }
    }

    override fun getItemCount() = list.size

    inner class QuoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        var tvAuthor: TextView = itemView.findViewById(R.id.tvAuthor)
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Quote)
    }

}