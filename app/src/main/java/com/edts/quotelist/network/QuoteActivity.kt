package com.edts.quotelist.network

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.edts.quotelist.R
import com.edts.quotelist.network.response.Quote

class QuoteActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA = "extra_data"
    }

    private lateinit var tvTitle: TextView
    private lateinit var tvAuthor: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quote)

        tvTitle = findViewById(R.id.tvTitle)
        tvAuthor = findViewById(R.id.tvAuthor)

        val detailQuote = intent.getParcelableExtra<Quote>(EXTRA_DATA)
        showDetailQuote(detailQuote)

    }

    private fun showDetailQuote(quote: Quote?) {
        quote?.let {
            tvTitle.text = it.en
            tvAuthor.text = it.author
        }
    }
}