package com.edts.quotelist.network.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Quote(
    @field:SerializedName("_id")
    val id: String,

    @field:SerializedName("en")
    val en: String,

    @field:SerializedName("author")
    val author: String
) : Parcelable
