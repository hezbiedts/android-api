package com.edts.quotelist.network

import com.edts.quotelist.network.response.Quote
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("list")
    fun getList(): Call<List<Quote>>
}